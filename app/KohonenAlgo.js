import * as _ from 'lodash';

let LEARNING_RATE = 0.3;
const LEARNING_STEP = 0.05;
const OUTPUT_NEURONS = 2;


export class Kohonen {

  constructor(data) {

    this.data = data.items.map((item) => Object.values(item));
    this.vector_length = Object.keys(this.data[0]).length;
    this.weights = [];
    this.normalized_data = [];
    this.clusters = [[], []];


    // Init weights
    for(let i=0; i<OUTPUT_NEURONS; i++) {
      let weight_vector = new Array(this.vector_length - 1).fill(1/Math.sqrt(this.vector_length - 1));
      this.weights.push(weight_vector);
    }

    this.normalizeData();
    this.train();
  }

  normalizeData() {

    let temp_data = [];

    for(let i = 0; i < this.data[0].length - 1; i++) {

      let col = this.data.map((item)=>{
        return item[i];
      });

      let normalized_vector = col.map((value)=> {
        return (value -_.min(col))/(_.max(col) - _.min(col));
      });

      temp_data.push(normalized_vector);
    }

    for(let i = 0; i < temp_data[0].length; i++) {
      this.normalized_data.push(temp_data.map((item) => {
        return item[i];
      }))
    }

  }

  euclideanDistance(vector, target_vector) {

    let distance = 0;

    for(let i=0; i<vector.length; i++) {
      distance += Math.pow(vector[i] - target_vector[i], 2);
    }

    return Math.sqrt(distance)
  }


  train() {

    while(LEARNING_RATE > 0) {

      this.clusters = [[], []];
      let temp_data = this.normalized_data.map((item)=>{return item});
      let indices = this.normalized_data.map((item)=>{return item});
      do {
        let vector = _.sample(temp_data);

        let euclidian_results =  this.weights.map((weight_vector) => {
          return this.euclideanDistance(vector, weight_vector);
        });

        let winner_index = _.indexOf(euclidian_results, _.min(euclidian_results));
        this.clusters[winner_index].push(_.indexOf(indices, vector));

        this.updateWeight(vector, winner_index);

        _.remove(temp_data, (v)=> {
          return _.isEqual(v, vector);
        });

      }while(_.size(temp_data));

      LEARNING_RATE -= LEARNING_STEP;

    }
  }

  resolveVector(vector) {

    let distances = [];

    for(let i=0;i<_.size(this.weights); i++) {

      distances.push(this.euclideanDistance(vector, this.weights[i]));

    }

    return distances;
  }

  updateWeight(vector, winner_index) {

    let weight_vector = this.weights[winner_index];

    for(let i = 0; i < weight_vector.length; i++) {
     weight_vector[i] = weight_vector[i] + LEARNING_RATE * (vector[i] - weight_vector[i]);
    }
  }

}