import 'styles/index.scss';
import data from '../assets/inputs.json'
import table_template from '../table.hbs'
import {Kohonen} from './KohonenAlgo'
import clusters_template from '../clusters.hbs'

var khn = new Kohonen(data);

$('document').ready(() =>{
  khn.train();

  let context = {
    data: khn.data,
    clusters: khn.clusters
  };
  console.log(khn.clusters);

  let compiledtable = table_template(context);

  context['data'] = khn.normalized_data;
  let compiled_normalized = table_template(context);
  let compiled_clusters = clusters_template(context);
  $('.container').append(compiledtable).append(compiled_normalized).append(compiled_clusters);




});

$('#classify').click( ()=> {
  let divinity = $('#divinity').val();
  let bgate = $('#bgate').val();
  let d2 = $('#dota2').val();
  let lol = $('#lol').val();
  let overwatch = $('#overwatch').val();

  let input_vector = [divinity, bgate, d2, lol, overwatch];

  let result = khn.resolveVector(input_vector);

  alert(result);

});