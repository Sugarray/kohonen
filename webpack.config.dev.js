var webpackConfig = require('./webpack.config.js');

webpackConfig.devtool = '#source-map';

webpackConfig.output = {
    pathinfo: true,
    publicPath: '/',
    filename: '[name].[hash].js'
};

webpackConfig.debug = true;

module.exports = webpackConfig;
